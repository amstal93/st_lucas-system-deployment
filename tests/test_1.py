import pytest
import os
import json

class Test1:
    def test_1_001(self):
        """Check primary data.

        This tests case consists of checking that primary data was
        downloaded according to the configuration.
        """
        input_json = os.path.join(os.environ['CONF_DIR'], 'primary_data.json')
        with open(input_json) as json_file:
            data = json.load(json_file)
        for dirname in data.keys():
            target_dir = os.path.join(os.environ["DATA_DIR"], "primary", dirname)
            for url in data[dirname]:
                filename = os.path.join(target_dir, os.path.basename(url))
                # fix XLS files from 2006, content is CSV(!)
                if os.path.splitext(filename)[1] == '.xls':
                    filename = os.path.splitext(filename)[0] + '.csv'

                assert os.path.exists(filename) and os.path.isfile(filename)
