#!/bin/sh -e

export BUILDDIR=/opt/sphinx/build

# build main docs
cd /opt/docs
rm -rf tables_*_rst
python3 utils/list_of_attributes.py /opt/conf/list_of_attributes.csv /opt/conf/thematic_attributes.json tables/ 
python3 utils/csv2rst.py tables/ tables_rst/
make clean
make html

# build Python API docs
cd /opt/st_lucas-python-package/docs
git pull
make html
cp -r _build/html /opt/sphinx/build/html/api

# build QGIS plugin docs
cd /opt/st_lucas-qgis-plugin/help
git pull
git checkout st_lucas_docs
make html
cp -r build/html /opt/sphinx/build/html/qgis_plugin

# extracts
EXTRACTSDIR=$BUILDDIR/html/extracts/
mkdir $EXTRACTSDIR
cp /data/dump.sql.7z $EXTRACTSDIR/db_dump.sql.7z
echo "DB dump file copied: $EXTRACTSDIR"

exit 0
