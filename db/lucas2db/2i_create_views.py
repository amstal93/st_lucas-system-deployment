#!/usr/bin/env python3

from lib.parser_cli import Parser
from lib.create_views import CreateViews

if __name__ == "__main__":
    parser = Parser(years=False,
        args=[
            { 'dest': 'groups', 'metavar': 'groups', 'type': str,
              'help': 'Groups to be processed'
            },
            { 'dest': 'table', 'metavar': 'table', 'type': str,
              'help': 'Table name'
            },
            { 'dest': 'pkey', 'metavar': 'pkey', 'type': str,
              'help': 'Primary key',
            }
        ]
    )

    cv = CreateViews(parser.columns, parser.groups, parser.table, parser.pkey)
    cv.exclude = ["SPACE-TIME"]
    cv.build_sql()
